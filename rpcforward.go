package yrpcproxy

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"sync"
	"time"

	"gitlab.com/kicad99/ykit/goutil"

	"github.com/golang/glog"
	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"github.com/nats-io/nats.go"
	"github.com/yangjuncode/yrpcmsg"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"

	"go.uber.org/ratelimit"
)

var ErrNilSocket = errors.New("socket is nil")

var pingMsgBytes []byte

var LogRPC bool

func init() {
	pingMsg := yrpcmsg.Ymsg{Cmd: 4}
	pingMsgBytes, _ = pingMsg.Marshal()
}

//go:generate syncmap -name TforwardClientMap -pkg yrpcproxy map[uuid.UUID]*TforwardClient

//GlobalForwardWebsockets uuid -> *TforwardClient
var GlobalForwardWebsockets = TforwardClientMap{}

var errNoSuchSubscription = errors.New("no such subscription")

type TforwardClient struct {
	sync.RWMutex
	WsSocket    *websocket.Conn
	SessionUUID uuid.UUID

	UserName string
	UserUUID uuid.UUID

	Sys string

	ConnectTime time.Time

	LastSendTime time.Time
	LastRecvTime time.Time

	RateLimitNo int //op per second
	RateLimiter ratelimit.Limiter

	Meta map[string]string

	NatsSubs []*nats.Subscription

	//流式调用管理
	StreamManager *TrpcStreamManager

	pingCount uint32
}
type TfnTforwardClientDisconnect func(client *TforwardClient, err error)

var FnTforwardClientDisconnect TfnTforwardClientDisconnect

type FnRPCUnaryHook func(client *TforwardClient, reqpkt *yrpcmsg.Ymsg, result []byte, reqMeta, resMetaHeader, resMetaTrailer metadata.MD, resErr error)

func (this *TforwardClient) Processing() {
	defer this.DisconnectClearUp()
	for {
		_ = this.WsSocket.SetReadDeadline(time.Now().Add(time.Minute * 6))
		_, msg, err := this.WsSocket.ReadMessage()

		var Errclose *websocket.CloseError
		if errors.As(err, &Errclose) {
			//ws has close
			//glog.Info("ws closed:", Errclose)
			if FnTforwardClientDisconnect != nil {
				FnTforwardClientDisconnect(this, err)
			}
			_ = this.WsSocket.Close()
			return
		}

		if err != nil {
			//glog.Info("ws read err:", err)
			if FnTforwardClientDisconnect != nil {
				FnTforwardClientDisconnect(this, err)
			}
			_ = this.WsSocket.Close()
			return
		}

		ymsg := &yrpcmsg.Ymsg{}
		err = ymsg.Unmarshal(msg)
		if err != nil {

			continue
		}

		//glog.Info("got ymsg:", ymsg)

		switch ymsg.Cmd {
		case 1:
			//Unary RPC
			go this.UnaryCall(ymsg)
		case 2:
			//Unary RPC(one way)
			go this.NocareCall(ymsg)
		case 3:
			//client stream start
			fallthrough
		case 7:
			//server stream start
			fallthrough
		case 8:
			//bidi stream start
			stream := this.NewRPCStreamCall(ymsg)
			if stream != nil {
				this.StreamManager.NewRPCStream(stream)
			}
		case 4:
			//cancel stream
			fallthrough
		case 5:
			//client stream middle send
			fallthrough
		case 6:
			//client stream finish
			fallthrough
		case 12:
			//response server stream msg recv
			fallthrough
		case 13:
			//response server stream eof received
			stream := this.StreamManager.GetRPCStream(ymsg)
			if stream != nil {
				stream.GotPacket(ymsg)
			}

		case 9:
			//nats publish
			this.OnNatsPub(ymsg)

		case 10:
			//nats sub unsub
			this.OnNatsSubUnsub(ymsg)

		case 14:
			this.pingCount++
			if this.pingCount%10 == 9 {
				this.StreamManager.CheckDeadStream()
			}
			this.wsPing(ymsg)
			if ymsg.Cid > 0 && ymsg.Cid == ymsg.No {
				//stream ping
				stream := this.StreamManager.GetRPCStream(ymsg)
				if stream != nil {
					stream.GotPacket(ymsg)
				}
			}

		}
	}
}
func (this *TforwardClient) DisconnectClearUp() {
	for _, sub := range this.NatsSubs {
		_ = sub.Unsubscribe()
	}
	if this.StreamManager != nil {
		this.StreamManager.ClearAllStream()
	}

	//sid, oksid := this.Meta["ykit-sid"]
	//sys, oksys := this.Meta["ykit-sys"]
	//if oksid && oksys {
	//	db, err := pgxdb.GetDbConn(sys)
	//	if err != nil {
	//		return
	//	}
	//	defer pgxdb.ReleaseDbConn(db)
	//	usql := "update dbusersession set updatedat = now_utc() where rid = $1;"
	//	_, _ = db.Exec(context.Background(), usql, sid)
	//}
	GlobalForwardWebsockets.Delete(this.SessionUUID)
}
func (this *TforwardClient) OnNatsPub(msg *yrpcmsg.Ymsg) {
	natsconn, err := goutil.FindNatsConn(this.Sys)
	if err != nil {
		this.ResponseErr(msg, err)
		return
	}
	if len(msg.Optbin) > 0 {
		reply := goutil.BytesToStrUnsafe(msg.Optbin)
		nmsg := &nats.Msg{
			Subject: msg.Optstr,
			Reply:   reply,
			Data:    msg.Body,
		}
		_ = natsconn.PublishMsg(nmsg)
	} else {
		_ = natsconn.Publish(msg.Optstr, msg.Body)
	}
}
func (this *TforwardClient) OnNatsSubUnsub(msg *yrpcmsg.Ymsg) {
	if len(msg.Optstr) == 0 {
		return
	}

	switch msg.Res {
	case 1:
		//sub
		hasSub := false
		for _, sub := range this.NatsSubs {
			if sub.Subject == msg.Optstr {
				hasSub = true
				break
			}
		}
		if hasSub {
			_ = this.WriteYmsg(msg)
			break
		}
		natsconn, err := goutil.FindNatsConn(this.Sys)
		if err != nil {
			this.ResponseErr(msg, err)
			break
		}
		sub, err := natsconn.Subscribe(msg.Optstr, func(msg *nats.Msg) {
			go this.OnNatsMsg(msg)
		})
		if err != nil {
			this.responseRPCErr(msg, err)
			//fmt.Println("nats sub failed:", msg.Optstr, this.UserName, this.UserUUID)

			break
		} //else {
		//fmt.Println("nats sub ok:", msg.Optstr, this.UserName, this.UserUUID)
		//}
		this.NatsSubs = append(this.NatsSubs, sub)
	case 2:
		//unsub
		unsubOK := false
		for i, sub := range this.NatsSubs {
			if sub.Subject == msg.Optstr {
				this.DelNatsSubscription(i)
				unsubOK = true
				break
			}
		}

		if unsubOK {
			//response ok
			_ = this.WriteYmsg(msg)
		} else {
			this.responseRPCErr(msg, errNoSuchSubscription)
		}
	}
}

func (this *TforwardClient) OnNatsMsg(msg *nats.Msg) {
	ymsg := &yrpcmsg.Ymsg{
		Cmd:    11,
		Body:   msg.Data,
		Optstr: msg.Subject,
	}
	if len(msg.Reply) > 0 {
		ymsg.Optbin = goutil.StrToBytesUnsafe(msg.Reply)
	}
	//fmt.Println("on nats msg:", this.UserName, this.UserUUID, msg.Subject)
	_ = this.WriteYmsg(ymsg)
}

// 下发15推送消息
func (this *TforwardClient) BroadcastMsg(optStr string, optBin, data []byte) {
	ymsg := &yrpcmsg.Ymsg{
		Cmd:    15,
		Body:   data,
		Optstr: optStr,
	}
	ymsg.Optbin = optBin
	//fmt.Println("on nats msg:", this.UserName, this.UserUUID, msg.Subject)
	_ = this.WriteYmsg(ymsg)
}

func (this *TforwardClient) DelNatsSubscription(i int) {
	_ = this.NatsSubs[i].Unsubscribe()
	this.NatsSubs[i] = this.NatsSubs[len(this.NatsSubs)-1]
	this.NatsSubs = this.NatsSubs[:len(this.NatsSubs)-1]
}

func (this *TforwardClient) MakeGrpcMD(msg *yrpcmsg.Ymsg) metadata.MD {
	this.RLock()
	md := metadata.New(this.Meta)
	this.RUnlock()
	msg.CopyMeta(&md)
	return md
}

func (this *TforwardClient) IPInfo() string {
	ws := this.WsSocket
	if ws == nil {
		return ""
	} else {
		return ws.RemoteAddr().String()
	}
}

//UnaryCall UnaryCall
func (this *TforwardClient) UnaryCall(msg *yrpcmsg.Ymsg) {
	//startTime := time.Now()
	ver := msg.MetaVer()
	api := msg.Optstr
	grpcConn, err := FindGrpcConn(api, ver, this.Sys)

	if err != nil {
		this.ResponseErr(msg, err)
		return
	}

	md := this.MakeGrpcMD(msg)
	ctx := metadata.NewOutgoingContext(context.Background(), md)

	var replyB []byte

	var header, trailer metadata.MD

	replyB, err = grpcConn.InvokeForward(ctx, api, msg.Body,
		grpc.Header(&header),   // will retrieve header
		grpc.Trailer(&trailer), // will retrieve trailer
	)
	if LogRPC {
		fmt.Println(goutil.NowTimeStrInLocal(), this.IPInfo(), this.UserName, "UnaryCall:", api, "err:", err)
	}
	if err != nil {
		//fmt.Println(goutil.NowTimeStrInLocal(),msg.Optstr, startTime.Format(goutil.ISOTimeFormat), goutil.NowTimeStrInLocal())
		this.ResponseErr(msg, err)
		return
	} else {
		this.responseRPCUnary(msg, replyB, header, trailer)
	}
}

func (this *TforwardClient) NocareCall(msg *yrpcmsg.Ymsg) {
	ver := msg.MetaVer()
	api := msg.Optstr
	grpcConn, err := FindGrpcConn(api, ver, this.Sys)

	if err != nil {
		return
	}

	md := this.MakeGrpcMD(msg)
	ctx := metadata.NewOutgoingContext(context.Background(), md)

	_, err = grpcConn.InvokeForward(ctx, msg.Optstr, msg.Body)
	if LogRPC {
		fmt.Println(goutil.NowTimeStrInLocal(), this.IPInfo(), this.UserName, "NocareCall:", api, "err:", err)
	}
}

func (this *TforwardClient) NewRPCStreamCall(pkt *yrpcmsg.Ymsg) (stream *TyrpcStream) {
	ver := pkt.MetaVer()
	api := pkt.Optstr
	grpcConn, err := FindGrpcConn(api, ver, this.Sys)

	if err != nil {
		this.ResponseErr(pkt, err)
		return
	}

	conn := this.WsSocket

	sreamDesc := &grpc.StreamDesc{
		ClientStreams: pkt.Cmd == 3 || pkt.Cmd == 8,
		ServerStreams: pkt.Cmd == 7 || pkt.Cmd == 8,
	}

	md := this.MakeGrpcMD(pkt)
	ctx, cancelFn := context.WithCancel(metadata.NewOutgoingContext(context.Background(), md))
	rpcStream, err := grpc.NewClientStream(ctx, sreamDesc, grpcConn, pkt.Optstr)
	if err != nil {
		fmt.Println("make rpc stream err:", err, pkt.Optstr)
		cancelFn()
		this.responseRPCErr(pkt, err)
		return nil
	}

	nowTime := time.Now()
	stream = &TyrpcStream{
		UID:          goutil.NewUUIDv1(),
		RPCType:      pkt.Cmd,
		Cid:          pkt.Cid,
		WsConn:       conn,
		RPCClient:    this,
		GrpcConn:     grpcConn,
		GrpcStream:   rpcStream,
		CancelFn:     cancelFn,
		API:          pkt.Optstr,
		LastYrpcTime: nowTime,
		LastGrpcTime: nowTime,
	}

	err = stream.GrpcStream.SendMsgForward(pkt.Body)
	if err != nil {
		fmt.Println("make rpc stream err:", err, pkt.Optstr)
		this.responseRPCErr(pkt, err)
		stream.CancelFn()
		stream.CancelFn = nil
		return nil
	}

	stream.RPCCallEstablished(pkt)

	if stream.RPCType == 7 {
		//server stream
		stream.CloseCliStream()
	}

	if LogRPC {
		fmt.Println(goutil.NowTimeStrInLocal(), this.IPInfo(), this.UserName, "StreamCall start:", api)
	}

	go stream.Recv()

	return stream
}

func (this *TforwardClient) responseRPCErr(pkt *yrpcmsg.Ymsg, err error) {
	pkt.Res = -1
	pkt.Optstr += err.Error()
	pkt.Cmd = 4

	_ = this.WriteYmsg(pkt)

}

func (this *TforwardClient) responseRPCUnary(reqpkt *yrpcmsg.Ymsg, result []byte, header, trailer metadata.MD) {
	for key, strs := range trailer {
		if len(strs) > 0 && strings.HasPrefix(key, "ykit-") {
			this.Lock()
			if key == "ykit-sys" && (reqpkt.Optstr == "/user.RpcUser/Login" || reqpkt.Optstr == "/base.Logon/Login") {
				this.Sys = strs[0]
			}
			this.Meta[key] = strs[0]
			this.Unlock()
		}
	}
	for key, strs := range header {
		if len(strs) > 0 && strings.HasPrefix(key, "ykit-") {
			this.Lock()
			if key == "ykit-sys" && (reqpkt.Optstr == "/user.RpcUser/Login" || reqpkt.Optstr == "/base.Logon/Login") {
				this.Sys = strs[0]
			}
			this.Meta[key] = strs[0]
			this.Unlock()
		}
	}

	reqpkt.Body = result
	reqpkt.Res = 0
	reqpkt.Optstr = ""

	if header.Len() > 0 || trailer.Len() > 0 {
		hmeta := yrpcmsg.GrpcMD2YrpcMeta(header)
		tmeta := yrpcmsg.GrpcMD2YrpcMeta(trailer)
		grpcMeta := yrpcmsg.GrpcMeta{
			Header:  &hmeta,
			Trailer: &tmeta,
		}
		optbin, _ := grpcMeta.Marshal()
		reqpkt.Optbin = optbin
	}

	//fmt.Println("response unary call:", reqpkt)

	_ = this.WriteYmsg(reqpkt)
}

func (this *TforwardClient) Ping() {
	ws := this.WsSocket
	if ws != nil {
		_ = this.Write(pingMsgBytes)
	}
}

func (this *TforwardClient) ResponseErr(msg *yrpcmsg.Ymsg, err error) {
	msg.Res = -1
	msg.Optstr += err.Error()
	msg.Cmd = 4

	errW := this.WriteYmsg(msg)
	if errW != nil {
		glog.Error("response err not ok:", msg, err, errW)
	} else {
		glog.Error("response err ok:", msg, err)
	}
}

func (this *TforwardClient) Write(data []byte) error {
	ws := this.WsSocket
	if ws == nil {
		return ErrNilSocket
	}
	this.Lock()
	defer this.Unlock()
	return writeWebsocketData(this.WsSocket, data)
}
func (this *TforwardClient) WriteYmsg(msg *yrpcmsg.Ymsg) error {
	ws := this.WsSocket
	if ws == nil {
		return ErrNilSocket
	}
	data, err := msg.Marshal()
	if err != nil {
		return err
	}
	this.Lock()
	defer this.Unlock()
	return writeWebsocketData(this.WsSocket, data)
}

func (this *TforwardClient) wsPing(ping *yrpcmsg.Ymsg) {
	if ping.Res != 0 {
		return
	}

	ping.Res = 1

	if len(ping.Optstr) > 0 {
		nowTime := time.Now()
		unixTime := &yrpcmsg.UnixTime{
			TimeUnix: goutil.GetUnixEpochInMilliseconds(nowTime),
			TimeStr:  goutil.GetUtcTimeStrzzz(nowTime),
		}

		ping.Body, _ = unixTime.Marshal()
	}

	_ = this.WriteYmsg(ping)

}

//对所有的客户端连接下发消息
func BroadcastAllClientMsg(optStr string, optBin, data []byte, useGoRoutine bool) {
	GlobalForwardWebsockets.Range(func(key uuid.UUID, client *TforwardClient) bool {
		if useGoRoutine {
			go client.BroadcastMsg(optStr, optBin, data)
		} else {
			client.BroadcastMsg(optStr, optBin, data)
		}
		return true
	})
}
