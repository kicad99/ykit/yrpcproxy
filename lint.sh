#!/bin/sh

export GOPROXY=https://goproxy.cn,direct
export GOPRIVATE=git.kicad99.com

go vet ./...
golangci-lint run -D govet -E maligned -E prealloc
staticcheck
