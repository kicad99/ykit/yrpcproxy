//Package yrpcproxy gorilla websocket 相关代码
package yrpcproxy

import (
	"net/http"
	"time"

	"github.com/gorilla/websocket"
	"github.com/yangjuncode/yrpcmsg"
	"gitlab.com/kicad99/ykit/goutil"
	"go.uber.org/ratelimit"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

//ProxyHandlerGorilla yrpc-proxy http handler
func ProxyHandlerGorilla(w http.ResponseWriter, r *http.Request) {
	// Upgrade connection
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		return
	}

	connectTime := time.Now()
	errCount := 0

	beforeLoginRateLimiter := ratelimit.New(5)

	// Read messages from ws socket
	for {
		beforeLoginRateLimiter.Take()
		//must send ping in 10 s
		_ = conn.SetReadDeadline(time.Now().Add(time.Second * 10))
		_, msg, err := conn.ReadMessage()
		if err != nil {
			_ = conn.Close()
			return
		}

		ymsg := &yrpcmsg.Ymsg{}
		err = ymsg.Unmarshal(msg)
		if err != nil {
			errCount++
			if errCount > 3 {
				_ = conn.Close()
				return
			}
			continue
		}

		switch ymsg.Cmd {
		case 14:
			nowTime := time.Now()
			client := &TforwardClient{
				WsSocket:      conn,
				SessionUUID:   goutil.NewUUIDv1(),
				LastSendTime:  nowTime,
				LastRecvTime:  nowTime,
				ConnectTime:   connectTime,
				StreamManager: TrpcStreamManagerNew(),
				Meta:          map[string]string{},
			}
			client.wsPing(ymsg)
			client.Meta["ykit-ipinfo"] = conn.RemoteAddr().String()

			GlobalForwardWebsockets.Store(client.SessionUUID, client)
			go client.Processing()
			return
		default:
			//need send ping first
			//wsErrStr(conn, ymsg, -100, "comm init err")

		}

	}
}

func writeWebsocketData(ws *websocket.Conn, data []byte) error {
	return ws.WriteMessage(websocket.BinaryMessage, data)
}
