package yrpcproxy

import (
	"errors"
	"gitlab.com/kicad99/ykit/goutil"
	"google.golang.org/grpc"
	"google.golang.org/grpc/connectivity"
	"google.golang.org/grpc/credentials/insecure"
)

var grpcSysAddrs = goutil.TstrMap{}
var DefaultGrpcSys string

//go:generate syncmap -name TgrpcConMap -pkg yrpcproxy map[string]*grpc.ClientConn
//sys -> *grpc.ClientConn
var grpcConnCaches = TgrpcConMap{}

//SetGrpcAddr 在没有命名发现之前，使用函数设置唯一的一个grpc server地址
//grpc.Dial(grpcSysAddr, grpc.WithInsecure())
func SetGrpcAddr(sys, grpcaddr string) {
	grpcSysAddrs.Store(sys, grpcaddr)
	if len(DefaultGrpcSys) == 0 {
		//将第一个添加的作为默认sys
		DefaultGrpcSys = sys
	}
}

//FindGrpcConn 给定api和版本，找相应的gprc-conn
//fixme 目前实现的是简化版，一个sys只有一个gprc con,以后应该是有pool的,单连接的throughout是有限的
func FindGrpcConn(api string, ver string, system string) (conn *grpc.ClientConn, err error) {
	if len(system) == 0 {
		system = DefaultGrpcSys
	}
	cacheConn, ok := grpcConnCaches.Load(system)
	if ok {
		conn = cacheConn
		if conn.GetState() == connectivity.Ready {
			return conn, nil
		} else {
			_ = conn.Close()
			grpcConnCaches.Delete(system)
		}
	}
	grpcaddr, ok := grpcSysAddrs.Load(system)
	if !ok {
		return nil, errors.New("now grpc url config for:" + system)
	}
	grpConn, err := grpc.Dial(grpcaddr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	grpcConnCaches.Store(system, grpConn)

	return grpConn, nil
}
