//Package yrpcproxy yrpc stream 相关功能
package yrpcproxy

import (
	"context"
	"fmt"
	"gitlab.com/kicad99/ykit/goutil"
	"go.uber.org/atomic"
	"io"
	"sync"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"github.com/yangjuncode/yrpcmsg"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

type TyrpcStream struct {
	LastYrpcTime time.Time
	LastGrpcTime time.Time
	GrpcStream   grpc.ClientStream
	API          string
	GrpcConn     *grpc.ClientConn
	CancelFn     context.CancelFunc
	WsConn       *websocket.Conn
	RPCClient    *TforwardClient
	manager      *TrpcStreamManager
	HasCancel    atomic.Bool

	Cid        uint32
	RPCType    uint32
	responseNo uint32
	UID        uuid.UUID

	HasCloseSent bool
}

func (this *TyrpcStream) IPInfo() string {
	ws := this.WsConn
	if ws == nil {
		return ""
	} else {
		return ws.RemoteAddr().String()
	}
}
func (this *TyrpcStream) RPCCallEstablished(pkt *yrpcmsg.Ymsg) {
	resPkt := &yrpcmsg.Ymsg{
		Cid: pkt.Cid,
		Cmd: pkt.Cmd,
		No:  pkt.No,
	}

	_ = this.WriteYmsg(resPkt)
}
func (this *TyrpcStream) Cancel() {
	if this.HasCancel.Load() {
		return
	}
	this.manager.DestroyRPCStream(this)
	msg := &yrpcmsg.Ymsg{
		Cmd: 44,
		Cid: this.Cid,
	}
	_ = this.WriteYmsg(msg)
	this.HasCancel.Store(true)
	if this.CancelFn != nil {
		//fmt.Println("got cancel and cancelfn is ok", this.Cid, this.API)
		//fmt.Printf("stream obj p:%p, cancelfn:%p\n", this, this.CancelFn)
		cancel := this.CancelFn
		if cancel != nil {
			cancel()
		}
		this.CancelFn = nil
	} //else {
	//fmt.Println("got cancel but cancelfn is nil", this.Cid, this.API)
	//fmt.Printf("stream obj p:%p, cancelfn:%p\n", this, this.CancelFn)
	//}
}
func (this *TyrpcStream) CloseCliStream() {
	if this.HasCloseSent || this.HasCancel.Load() {
		return
	}
	_ = this.GrpcStream.CloseSend()
}
func (this *TyrpcStream) GotPacket(pkt *yrpcmsg.Ymsg) {
	if this.HasCancel.Load() {
		return
	}
	this.LastYrpcTime = time.Now()
	switch pkt.Cmd {
	case 4:
		//cancel
		this.Cancel()
		this.manager.DestroyRPCStream(this)
	case 5:
		_ = this.SendNext(pkt)
	case 6:
		this.CloseCliStream()
	case 12:
		//client has recv reply
	case 13:
		this.manager.DestroyRPCStream(this)
	case 14:
		//stream ping
	}
}
func (this *TyrpcStream) SendHeader(md metadata.MD) {
	if md.Len() == 0 {
		return
	}
	meta := yrpcmsg.GrpcMD2YrpcMeta(md)

	if len(meta.Val) == 0 {
		return
	}

	pkt := &yrpcmsg.Ymsg{
		Cmd:      2,
		Cid:      this.Cid,
		MetaInfo: &meta,
	}
	_ = this.WriteYmsg(pkt)
}
func (this *TyrpcStream) WriteYmsg(msg *yrpcmsg.Ymsg) error {
	if this.HasCancel.Load() {
		return nil
	}
	return this.RPCClient.WriteYmsg(msg)
}
func (this *TyrpcStream) Recv() {
	header, _ := this.GrpcStream.Header()
	this.SendHeader(header)

	//fmt.Printf("recv stream obj %s p:%p, cancelfn:%p\n", this.API, this, this.CancelFn)

	for {

		msgB, err := this.GrpcStream.RecvMsgForward()
		if this.HasCancel.Load() {
			break
		}

		if err == io.EOF {
			this.RPCEnd()
			break
		}
		if err != nil {
			this.RPCClient.responseRPCErr(&yrpcmsg.Ymsg{
				Cid: this.Cid,
			}, err)

			this.manager.DestroyRPCStream(this)
			break
		}

		resPkt := &yrpcmsg.Ymsg{
			Cmd:  12,
			Cid:  this.Cid,
			No:   this.GetResponseNo(),
			Body: msgB,
		}
		err = this.WriteYmsg(resPkt)
		if err != nil {
			fmt.Println("send ws response 12 err:", this.API)
		}
	}

	if this.CancelFn != nil {
		cancel := this.CancelFn
		if cancel != nil {
			//fmt.Printf("stream finished api:%s cid:%d streamp:%p cancelfn:%p \n", this.API, this.Cid, this, this.CancelFn)
			cancel()
			this.CancelFn = nil
		}
	}

	if LogRPC {
		fmt.Println(goutil.NowTimeStrInLocal(), this.IPInfo(), "StreamCall Finished:", this.API)
	}

}
func (this *TyrpcStream) GetResponseNo() uint32 {
	r := this.responseNo
	this.responseNo++
	return r
}
func (this *TyrpcStream) RPCEnd() {
	md := this.GrpcStream.Trailer()
	pkt := &yrpcmsg.Ymsg{
		Cmd: 13,
		Cid: this.Cid,
	}
	if md.Len() > 0 {
		meta := yrpcmsg.GrpcMD2YrpcMeta(md)
		if len(meta.Val) > 0 {
			pkt.MetaInfo = &meta
		}
	}

	_ = this.WriteYmsg(pkt)

}
func (this *TyrpcStream) SendNext(pkt *yrpcmsg.Ymsg) (err error) {
	if this.HasCloseSent {
		return
	}
	err = this.GrpcStream.SendMsgForward(pkt.Body)
	if err != nil {
		return err
	}

	return nil
}

type TrpcStreamManager struct {
	//cid -> *TyrpcStream
	Streams sync.Map
}

func TrpcStreamManagerNew() *TrpcStreamManager {
	return &TrpcStreamManager{
		Streams: sync.Map{},
	}
}
func (this *TrpcStreamManager) CheckDeadStream() {
	this.Streams.Range(func(key, value interface{}) bool {
		stream := value.(*TyrpcStream)
		if time.Since(stream.LastYrpcTime) > 12*time.Minute {
			fmt.Println("cancel dead rpc stream:", stream.API, stream.LastYrpcTime)
			stream.Cancel()
		}
		return true
	})
}
func (this *TrpcStreamManager) GetRPCStream(pkt *yrpcmsg.Ymsg) (stream *TyrpcStream) {
	s, exists := this.Streams.Load(pkt.Cid)
	if !exists {
		return nil
	}

	return s.(*TyrpcStream)
}
func (this *TrpcStreamManager) NewRPCStream(stream *TyrpcStream) {
	oldS, exists := this.Streams.Load(stream.Cid)
	if exists {
		oldstream := oldS.(*TyrpcStream)
		if oldS != stream {
			oldstream.Cancel()
		}
	}
	stream.manager = this
	this.Streams.Store(stream.Cid, stream)
}
func (this *TrpcStreamManager) DestroyRPCStream(stream *TyrpcStream) {
	this.Streams.Delete(stream.Cid)
}
func (this *TrpcStreamManager) ClearAllStream() {
	this.Streams.Range(func(key, value interface{}) bool {
		s := value.(*TyrpcStream)
		s.Cancel()
		return true
	})
}
