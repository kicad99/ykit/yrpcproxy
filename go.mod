module gitlab.com/kicad99/ykit/yrpcproxy

go 1.13

require (
	github.com/golang/glog v1.0.0
	github.com/google/uuid v1.3.0
	github.com/gorilla/websocket v1.5.0
	github.com/nats-io/nats.go v1.13.1-0.20220121202836-972a071d373d
	github.com/yangjuncode/yrpcmsg v1.2.5
	gitlab.com/kicad99/ykit/goutil v1.12.2
	go.uber.org/atomic v1.9.0
	go.uber.org/ratelimit v0.2.0
	google.golang.org/grpc v1.44.0

)

replace google.golang.org/grpc => github.com/yangjuncode/grpc-go v1.45.0
